<?php 
include('inc/application_top.php');
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center pt20">
				<h1><i class="fa fa-rocket"></i></h1>
				<h3>Front-end Template</h3>
				<h1>v3</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="text-center">
				<ul class="list-group">
					<li class="list-group-item"><a href="pages/ui-guide.php" rel="noopener noreferrer">UI Guide</a></li>
					<li class="list-group-item"><a href="pages/components/header.php" rel="noopener noreferrer">Navigation</a></li>
					<li class="list-group-item"><a href="pages/components/footer" rel="noopener noreferrer">Footer</a></li>
					<li class="list-group-item"><a href="pages/home.php" rel="noopener noreferrer">Home</a></li>
					<li class="list-group-item"><a href="pages/product-listing.php" rel="noopener noreferrer">Product Listing</a></li>
					<li class="list-group-item"><a href="pages/product-details.php" rel="noopener noreferrer">Product Details</a></li>
					<li class="list-group-item"><a href="pages/portfolio-listing.php" rel="noopener noreferrer">Portfolio Listing</a></li>
					<li class="list-group-item"><a href="pages/portfolio-details.php" rel="noopener noreferrer">Portfolio Details</a></li>
				</ul>		
			</div>
		</div>
	</div>
</div>
<?php 
include('inc/application_bottom.php');
?>