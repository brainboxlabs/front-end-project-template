<?php 
include('../inc/application_top.php');
?>
<?php 
include('./components/header.php');
?>
<main>
    <section class="hero bg-image" style="background-image: url('../images/hero-main.png');">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="hero-header">
                        <h1>Would you like to install Your Dream Space?</h1>
                        <svg width="34px" height="3px" viewBox="0 0 34 3" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                        
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                                <g id="home---hover" transform="translate(-1040.000000, -1586.000000)" fill-rule="nonzero" stroke="#5BB456" stroke-width="2">
                                    <path d="M1041.5,1587.25 L1072.5,1587.25" id="Line-17-Copy-2"></path>
                                </g>
                            </g>
                        </svg>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quis quo eos ducimus perspiciatis esse molestiae rerum delectus saepe culpa aut accusantium dignissimos, aliquam similique modi quaerat id magnam exercitationem.</p>
                        <a href="" class="btn btn-primary">See Featured Product</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cta bg-black">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h6>We provide great shipping options for some items</h6>
                </div>
                <div class="col-md-6 text-right">
                    <a href="" class="btn btn-primary">See All Shipping Details</a>
                </div>                          
            </div>
        </div>
    </section>
    <section class="gallery clearfix">
        <div class="gallery-item" style="background-image: url('../images/banner-section-one.png');">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="gallery-item-content">
                            <h1 class="display-two"><span class="highlight-black">Sole Treadmill <span class="colored-secondary">Summer Sale</span></span></h1>
                            <h1 class="display-two"><span class="highlight-black">up to 50% off</span></h1>
                            <p><span class="highlight-black">30% Off when you purchased over $5000 by June 15th. Don't miss it! It's only limited quantity. <a href="">Shop Now</a></span></p>
                        </div>                      
                    </div>
                </div>
            </div>        
        </div>
        <div class="gallery-row-middle">
            <div class="gallery-item gallery-item-two" style="background-image: url('../images/banner-section-two.png');">
                <div class="container">
                    <div class="gallery-item-content">            
                        <h1 class="display-two"><span class="highlight-black">Rubber Flooring</span></h1>
                        <h1 class="display-two"><span class="highlight-black">Summer Sale</span></h1>
                        <h1 class="display-two"><span class="highlight-black">up to 50% off</span></h1>
                        <p><span class="highlight-black">30% Off when you purchased over $5000 by June 15th. <a href="">Shop Now</a></span></p>
                    </div>                        
                </div>                 
            </div>
            <div class="gallery-item gallery-item-three" style="background-image: url('../images/banner-section-three.png');">
                <div class="container">
                    <div class="gallery-item-content">            
                        <h1 class="display-two"><span class="highlight-black">Strength Equipment</span></h1>
                        <h1 class="display-two"><span class="highlight-black">up to 50% off</span></h1>
                        <p><span class="highlight-black">30% Off when you purchased over $5000 by June 15th. <a href="">Shop Now</a></span></p>
                    </div>                        
                </div>                 
            </div>       
        </div>
        <div class="gallery-row-bottom">
            <div class="gallery-item gallery-item-four" style="background-image: url('../images/banner-section-four.png');">
                <div class="container">
                    <div class="gallery-item-content">            
                        <h1 class="display-two"><span class="highlight-black colored-secondary">Trade up &amp; Save</span></h1>
                        <h1 class="display-two"><span class="highlight-black">Fitness Equipments</span></h1>                    
                        <a href=""><span class="highlight-black">Shop Now</span></a>
                    </div>                        
                </div>                 
            </div>
            <div class="gallery-item gallery-item-five" style="background-image: url('../images/banner-section-five.png');">
                <div class="container">
                    <div class="gallery-item-content">                                
                        <h1 class="display-two"><span class="highlight-black">Dumbbells and Plates</span></h1>                    
                        <p><span class="highlight-black">30% Off when you purchased over $5000 by June 15th. <a href="">Shop Now</a></span></p>
                    </div>                        
                </div>                 
            </div>
        </div>        
    </section>
    <section class="products clearfix bg-secondary">
        <div class="container">
            <div class="section-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Featured Products</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="">SEE ALL PRODUCTS</a>
                    </div>
                </div>
            </div>        
            <div class="products-main">
                <div class="row">
                    <div class="col-md-3">
                        <a class="product-link" href="">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>
                    <div class="col-md-3">
                        <a class="product-link" href="">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>
                    <div class="col-md-3">
                        <a href="" class="product-link">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>
                    <div class="col-md-3">
                        <a class="product-link" href="">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>
                    <div class="col-md-3">
                        <a class="product-link" href="">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>
                    <div class="col-md-3">
                        <a class="product-link" href="">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>
                    <div class="col-md-3">
                        <a class="product-link" href="">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>
                    <div class="col-md-3">
                        <a  class="product-link" href="">
                            <figure class="card card-product">
                                <div class="img-wrap"><img width="253px" src="../images/product-tile-1.png" alt=""></div>
                                <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                            <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                        </g>
                                    </g>
                                </svg>
                                <figcaption class="info-wrap p-3">
                                    <h4 class="title">Everlast Indoor Cycle EV</h4>
                                    <h4 class="title">673</h4>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <del>$799</del>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                $100.00/ea
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-949.000000, -2690.000000)" fill="#1FC16D">
                                                            <circle id="Oval-4-Copy-9" cx="959" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1096.000000, -2690.000000)" fill="#8E5A59">
                                                            <circle id="Oval-4-Copy-13" cx="1106" cy="2700" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="xl-1-home" transform="translate(-1180.000000, -3179.000000)" fill="#3A3A3A">
                                                            <circle id="Oval-4-Copy-16" cx="1190" cy="3189" r="10"></circle>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>          
                                    </div>                            
                                </figcaption>                                                
                            </figure>
                        </a>                        
                    </div>                                                                                                                                 
                </div>
            </div>
        </div>
    </section>
    <section class="concept">
        <div class="container">
            <div class="section-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Concept &amp; Design</h3>                        
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="">SEE MORE CONCEPT &amp; DESIGN</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum dicta reiciendis dolore facere accusantium odio nobis laudantium optio architecto, fuga est animi in repudiandae magni natus cumque ullam vero voluptatibus.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="concept-steps">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="../images/concept-step-one.png" width="120px" alt="">
                                </div>                                
                                <div class="col-md-3">
                                    <img src="../images/concept-step-two.png" width="120px" alt="">
                                </div>
                                <div class="col-md-3">
                                    <img src="../images/concept-step-three.png" width="120px" alt=""> 
                                </div>
                                <div class="col-md-3">
                                    <img src="../images/concept-step-four.png" width="120px" alt="">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <p class="lead"><mark>The first step is a consulation.</mark> We supply all the necessary tools for our clients, allowing them to focus on their member's development from beginners into well conditioned athletes.</p>
                        <svg width="50px" height="4px" viewBox="0 0 50 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                            <desc>Created with Sketch.</desc>
                            <defs></defs>
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="xl-1-home" transform="translate(-185.000000, -2556.000000)" fill="#82CB62">
                                    <rect id="Rectangle-2-Copy-4" x="185" y="2556" width="50" height="4"></rect>
                                </g>
                            </g>
                        </svg>                        
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis soluta accusamus odit consectetur similique. Neque voluptates omnis, atque unde velit dignissimos animi! Id dolorum nisi veritatis, at modi beatae blanditiis!</p>
                    </div>
                    <div class="col-md-4">
                        <div class="video-container">
                            <div class="img-wrap"> <img width="373px" height="256px" src="../images/video-image-homepage.png" alt=""> </div>
                            <div class="overlay"></div>
                            <a href="">
                                <svg id="play-arrow" width="78px" height="78px" viewBox="0 0 78 78" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="z-index: 20;">                                
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g  transform="translate(-1014.000000, -3542.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                            <g transform="translate(1014.000000, 3542.000000)">
                                                <path d="M38.9999086,0 C17.4525473,0 0,17.4525882 0,39 C0,60.5474118 17.4525473,78 38.9999086,78 C60.5472699,78 78,60.5475946 78,39 C78,17.4524054 60.5474527,0 38.9999086,0 Z M31.2000366,56.5500274 L31.2000366,21.4499726 L54.6000183,39 L31.2000366,56.5500274 Z" id="Shape"></path>
                                            </g>
                                        </g>
                                    </g>
                                </svg>                        
                            </a>
                        </div>                                                
                        <p class="pt-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>        
        </div>
    </section>
    <section class="testimonial bg-secondary pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <img src="../images/client-tml.png" alt="" width="161px" height="181px">
                </div>
                <div class="col-md-10">
                    <blockquote class="blockquote">
                        <p class="mb-0"><em>Thank you for all your hard work in making our facility come to life. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</em> </p>
                        <cite title="Toronto Maple Leafs">- Toronto Maple Leafs</cite>
                    </blockquote>
                </div>
            </div>
        </div>
    </section>
    <section class="cta cta-contact bg-tert">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-5 text-right">
                    <div class="contact-cta-copy">Feel free to call us <a href="tel:+18866471234">1.886.647.1234</a></div>
                </div>
                <div class="col-md-2 text-center">
                    <svg width="48px" height="48px" viewBox="0 0 48 48" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                        
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="xl-1-home" transform="translate(-696.000000, -4216.000000)">
                                <g id="OR" transform="translate(697.000000, 4217.000000)">
                                    <text id="OR-Copy" font-family="Roboto-Bold, Roboto" font-size="22" font-weight="bold" fill="#919191">
                                        <tspan x="8" y="28">OR</tspan>
                                    </text>
                                    <circle id="Oval-2-Copy" stroke="#919191" stroke-width="2" cx="23" cy="23" r="23"></circle>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="col-md-5 text-left">
                    <div class="contact-cta-copy">Email us <a href="mailto:info@landmarkathletics.com">info@landmarkathletics.com</a></div>
                </div>
            </div>            
        </div>        
    </section>
</main>


<?php 
include('../inc/application_bottom.php');
?>
<?php 
include('./components/footer.php');
?>