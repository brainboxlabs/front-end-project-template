<?php 
include('../inc/application_top.php');
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 pt-5">            
            <h1>H1.Roboto Bold 32px, 40line</h1>
            <h2>H2.Roboto Medium 30px, 38line</h2>
            <h3>H3.Roboto Bold 25px, 33line</h3>
            <h4>H4.Roboto Medium 23px, 31line</h4>
            <h5>H5.Roboto Bold 20px, 28line</h5>
            <h6>H6.Roboto Medium 20px, 28line</h6>                        
            <h1>H1.Roboto Bold 32px, 40line</h1>
            <h5>Roboto Medium 20px, 28line</h5>                                     
            <p class="lead">Lead Copy. Roboto medium 19px, 27line</p>
            <p>Body Copy. Roboto Regular 17px, 25line</p>
            <p class="bold">Body Copy. Bold Roboto Medium 17px, 25line</p>                
            <p class="small">Body Copy, Small Roboto Regular 15px, 23line</p>
            <em>Lead Copy. Emphasize</em>            
            <mark>Lead Copy.Marked</mark>                        
            <a href="#">Link Text</a>
        </div>
    </div>
</div>
<?php 
include('../inc/application_bottom.php');
?>