<?php 
include('../../inc/application_top.php');
?>
<header class="fixed-top">
    <nav class="navbar navbar-expand-lg bg-quad nav-cta animated">
        <div class="container">
            <ul class="nav navbar-cta justify-content-center">
                <li>Free Shipping on your 1st Order your $300 <a href="">See All Shipping Deals</a></li>
            </ul>
            <ul class="nav mr-auto">
                <li class="js-close-cta">
                    <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                        
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                            <g id="product-listing" transform="translate(-1260.000000, -13.000000)" stroke="#3A3A3A" stroke-width="2">
                                <g id="Group" transform="translate(1262.000000, 15.000000)">
                                    <path d="M0,10 L10,0" id="Line-5" transform="translate(5.000000, 5.000000) scale(-1, 1) translate(-5.000000, -5.000000) "></path>
                                    <path d="M0,-5.68434189e-14 L10,10" id="Line-5-Copy" transform="translate(5.000000, 5.000000) scale(-1, 1) translate(-5.000000, -5.000000) "></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </li>
            </ul>            
        </div>
    </nav>    
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="/images/logo-white.png" alt="">                         
            </a>            
            <ul class="nav nav-main justify-content-between">                                
                <li class="nav-item">
                    <form action="" class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <svg class="btn-cart-icon" width="20px" height="23px" viewBox="0 0 20 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                                                                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="search-icon" transform="translate(-405.000000, -502.000000)" stroke="#565656" stroke-width="2">
                                                <g id="Group-4-Copy-5" transform="translate(406.000000, 503.000000)">
                                                    <circle id="Oval-5" cx="8.5" cy="8.5" r="8.5"></circle>
                                                    <path d="M14,16 L18,21" id="Path-4" stroke-linecap="round"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <input type="search" class="form-control js-search" placeholder="Search">
                        </div>
                    </form>    
                </li>
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">shipping details</a></li>
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">contact us</a></li>
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">my account</a></li>
                <li class="nav-item dropdown">
                    <a class="btn btn-secondary" href="http://" target="_blank" rel="noopener noreferrer" data-toggle="dropdown">
                        <svg width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                        
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="icon-cart" transform="translate(-1157.000000, -63.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                    <g id="Group-10" transform="translate(1157.000000, 63.000000)">
                                        <g id="online-shopping">
                                            <path d="M9.20943529,16.0596048 C9.30948929,16.0596048 9.41079398,16.0532844 9.51227733,16.0404632 L19.8934165,14.7289028 C21.0944219,14.5772147 22,13.5404084 22,12.3172425 L22,5.50446004 C22,5.07612172 21.6564217,4.72877401 21.2325322,4.72877401 L16.3209525,4.72877401 C15.8971523,4.72877401 15.5534846,5.07603143 15.5534846,5.50446004 C15.5534846,5.93288865 15.8970629,6.28014607 16.3209525,6.28014607 L20.4650643,6.28014607 L20.4650643,12.3173328 C20.4650643,12.7597564 20.1375661,13.134733 19.7031352,13.1895394 L9.32199604,14.5010998 C8.92070801,14.5517528 8.54166413,14.3199769 8.39971251,13.9375062 L5.55853606,6.28014607 L9.77206041,6.28014607 C10.1958606,6.28014607 10.5395282,5.93288865 10.5395282,5.50446004 C10.5395282,5.07603143 10.1959499,4.72877401 9.77206041,4.72877401 L4.98295752,4.72877401 L4.34019986,2.99636941 C3.99027884,2.05310414 3.08541542,1.41935484 2.08871672,1.41935484 L0.76746783,1.41935484 C0.343667645,1.41935484 0,1.76661226 0,2.19504087 C0,2.62337919 0.343578311,2.9707269 0.76746783,2.9707269 L2.08871672,2.9707269 C2.44917914,2.9707269 2.77640934,3.19997458 2.90299553,3.54118253 L6.96241884,14.4823193 C7.06184751,14.7503919 7.20460313,14.9903842 7.37871497,15.1989553 L6.19325366,17.1959112 C6.02744988,17.1593436 5.85548206,17.1392991 5.6789582,17.1392991 C4.35306395,17.1392991 3.27435669,18.2295573 3.27435669,19.5696495 C3.27435669,20.9097417 4.35306395,22 5.6789582,22 C6.73658265,22 7.63617537,21.3060269 7.95733086,20.3452453 L17.3168472,20.3452453 C17.6379133,21.3060269 18.5375954,22 19.5952198,22 C20.9211141,22 21.9998213,20.9097417 21.9998213,19.5696495 C21.9998213,18.2295573 20.9211141,17.1392991 19.5952198,17.1392991 C18.5375954,17.1392991 17.6380027,17.8332721 17.3168472,18.7940538 L7.95733086,18.7940538 C7.85852753,18.498262 7.70487317,18.2279321 7.50860651,17.9952534 L8.69192381,16.0019091 C8.86005027,16.0392894 9.0330901,16.0596048 9.20943529,16.0596048 Z M5.67931554,20.4486279 C5.19968165,20.4486279 4.80956036,20.0543291 4.80956036,19.5695592 C4.80956036,19.0847893 5.19968165,18.6904905 5.67931554,18.6904905 C6.15894943,18.6904905 6.54907072,19.0847893 6.54907072,19.5695592 C6.54907072,20.0543291 6.1588601,20.4486279 5.67931554,20.4486279 Z M19.5953985,18.6904905 C20.0750324,18.6904905 20.4651537,19.0847893 20.4651537,19.5695592 C20.4651537,20.0543291 20.0750324,20.4486279 19.5953985,20.4486279 C19.1157646,20.4486279 18.7256433,20.0543291 18.7256433,19.5695592 C18.7256433,19.0848796 19.1157646,18.6904905 19.5953985,18.6904905 Z" id="Shape"></path>
                                            <path d="M16.0998614,8.33338855 C15.8029024,8.03643137 15.3214836,8.03643137 15.0246131,8.33338855 L13.889442,9.46855274 L13.889442,0.76040511 C13.889442,0.340505 13.5490234,0 13.1290323,0 C12.7091296,0 12.3686225,0.340416488 12.3686225,0.76040511 L12.3686225,9.46855274 L11.2334514,8.33338855 C10.9364924,8.03643137 10.4550736,8.03643137 10.1582031,8.33338855 C9.86124412,8.63034574 9.86124412,9.11167305 10.1582031,9.40863023 L12.5914081,11.8418204 C12.7398434,11.9902547 12.9344821,12.0645161 13.1290323,12.0645161 C13.3235824,12.0645161 13.5182211,11.9903432 13.6666564,11.8418204 L16.0998614,9.40863023 C16.3968204,9.11176157 16.3968204,8.63034574 16.0998614,8.33338855 Z" id="Shape"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>                        
                        <span class="item-count">4 item(s)</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-cart">
                        <div class="cart-header">Order Summary</div>                        
                        <svg width="34px" height="3px" viewBox="0 0 34 3" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                            
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                                <g id="home---hover" transform="translate(-1040.000000, -1586.000000)" fill-rule="nonzero" stroke="#5BB456" stroke-width="2">
                                    <path d="M1041.5,1587.25 L1072.5,1587.25" id="Line-17-Copy-2"></path>
                                </g>
                            </g>
                        </svg>
                        <div class="cart-total">                            
                            <div class="row">
                                <div class="col-sm-6 subtotal">subtotal</div>
                                <div class="col-sm-6 subtotal-amount text-right">$13,885.07</div>
                            </div>
                            <div class="row">                                
                                <div class="col-sm-12 subtotal-amount-saved text-right"><em>Saved Total $5084.40</em></div>
                            </div>                                                                                                                                                                
                        </div>
                        <hr>
                        <div class="cart-copy">
                            Do you have a promotional code? We’ll ask you to enter your claim code when it’s time to pay. 
                        </div>
                        <a href="" class="btn btn-primary">View Cart &amp; Checkout</a>
                    </div>
                </li>
            </ul>    
        </div>    
    </nav>
    <nav class="navbar navbar-expand-lg bg-dark">
        <div class="container">        
            <ul class="nav nav-middle justify-content-between">
                <li class="nav-item dropdown">
                    <a class="nav-link js-mega-menu-class" href="javascript:void(0)" target="_blank" rel="noopener noreferrer" data-toggle="dropdown" id="js-mega-menu-trig">shop 
                        <svg width="12px" height="6px" viewBox="0 0 12 6" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                            
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" fill-opacity="0.8">
                                <g id="home---hover" transform="translate(-233.000000, -326.000000)" fill="#FFFFFF" fill-rule="nonzero" stroke="#FFFFFF">
                                    <g id="Shop-Copy-2" transform="translate(184.000000, 317.000000)">
                                        <polygon id="Shape" points="50.6688242 9 50 9.67255814 55 15 60 9.67255814 59.3311758 9 55 13.6130233"></polygon>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>                    
                    <div class="dropdown-menu mega-dropdown mt-2" id="js-mega-menu-dropdown">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link" id="v-pills-cardio-tab" data-toggle="pill" href="#v-pills-cardio" role="tab" aria-controls="v-pills-cardio" aria-selected="true">Cardio
                                <div class="float-right">
                                    <svg width="7px" height="11px" viewBox="0 0 7 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="home---hover" transform="translate(-396.000000, -2338.000000)" fill="#565656" fill-rule="nonzero">
                                                <polygon id="Shape-Copy-30" points="397.324324 2338 396 2339.28333 400.351351 2343.5 396 2347.71667 397.324324 2349 403 2343.5"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </div>                                
                            </a>
                            <a class="nav-link" id="v-pills-strength-tab" data-toggle="pill" href="#v-pills-strength" role="tab" aria-controls="v-pills-strength" aria-selected="false">Strength Equipment
                                <div class="float-right">
                                    <svg width="7px" height="11px" viewBox="0 0 7 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="home---hover" transform="translate(-396.000000, -2338.000000)" fill="#565656" fill-rule="nonzero">
                                                <polygon id="Shape-Copy-30" points="397.324324 2338 396 2339.28333 400.351351 2343.5 396 2347.71667 397.324324 2349 403 2343.5"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </div>     
                            </a>
                            <a class="nav-link" id="v-pills-fitness-tab" data-toggle="pill" href="#v-pills-fitness" role="tab" aria-controls="v-pills-home" aria-selected="true">Fitness Accessories
                                <div class="float-right">
                                    <svg width="7px" height="11px" viewBox="0 0 7 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="home---hover" transform="translate(-396.000000, -2338.000000)" fill="#565656" fill-rule="nonzero">
                                                <polygon id="Shape-Copy-30" points="397.324324 2338 396 2339.28333 400.351351 2343.5 396 2347.71667 397.324324 2349 403 2343.5"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </div>     
                            </a>
                            <a class="nav-link" id="v-pills-conditioning-tab" data-toggle="pill" href="#v-pills-conditioning" role="tab" aria-controls="v-pills-conditioning" aria-selected="false">Strength &amp; Conditioning
                                <div class="float-right">
                                    <svg width="7px" height="11px" viewBox="0 0 7 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="home---hover" transform="translate(-396.000000, -2338.000000)" fill="#565656" fill-rule="nonzero">
                                                <polygon id="Shape-Copy-30" points="397.324324 2338 396 2339.28333 400.351351 2343.5 396 2347.71667 397.324324 2349 403 2343.5"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </div>     
                            </a>
                            <a class="nav-link" id="v-pills-martial-arts-tab" data-toggle="pill" href="#v-pills-martial-arts" role="tab" aria-controls="v-pills-martial-arts" aria-selected="true">Martial Arts &amp; Boxing
                                <div class="float-right">
                                    <svg width="7px" height="11px" viewBox="0 0 7 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="home---hover" transform="translate(-396.000000, -2338.000000)" fill="#565656" fill-rule="nonzero">
                                                <polygon id="Shape-Copy-30" points="397.324324 2338 396 2339.28333 400.351351 2343.5 396 2347.71667 397.324324 2349 403 2343.5"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </div>     
                            </a>
                            <a class="nav-link" id="v-pills-flooring-tab" data-toggle="pill" href="#v-pills-flooring" role="tab" aria-controls="v-pills-flooring" aria-selected="false">Flooring
                                <div class="float-right">
                                    <svg width="7px" height="11px" viewBox="0 0 7 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="home---hover" transform="translate(-396.000000, -2338.000000)" fill="#565656" fill-rule="nonzero">
                                                <polygon id="Shape-Copy-30" points="397.324324 2338 396 2339.28333 400.351351 2343.5 396 2347.71667 397.324324 2349 403 2343.5"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </div>     
                            </a>
                            <a class="nav-link" id="v-pills-clearance-tab" data-toggle="pill" href="#v-pills-clearance" role="tab" aria-controls="v-pills-clearance" aria-selected="true">Clearance &amp; Sale
                                <div class="float-right">
                                    <svg width="7px" height="11px" viewBox="0 0 7 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">                                        
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="home---hover" transform="translate(-396.000000, -2338.000000)" fill="#565656" fill-rule="nonzero">
                                                <polygon id="Shape-Copy-30" points="397.324324 2338 396 2339.28333 400.351351 2343.5 396 2347.71667 397.324324 2349 403 2343.5"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </div>     
                            </a>                                                        
                        </div>                                   
                    </div>                                                       
                </li>                                
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">featured</a></li>
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">gym flooring</a></li>
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">concept &amp; design</a></li>
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">solutions</a></li>
                <li class="nav-item"><a class="nav-link" href="http://" target="_blank" rel="noopener noreferrer">portfolio</a></li>
                <li class="nav-item"><a class="nav-link highlight" href="http://" target="_blank" rel="noopener noreferrer">get a quote</a></li>
                <li class="nav-item">
                    <a data-toggle="collapse" id="js-nav-bottom-hook" href="#js-nav-bottom" target="_blank" rel="noopener noreferrer">                        
                        <button class="hamburger hamburger--vortex" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </a>
                </li>
            </ul>    
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane open fade show" id="v-pills-cardio" role="tabpanel" aria-labelledby="v-pills-cardio-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Cardio</h5>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="link-underline" href="#">View all Cardio Items</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Benches &amp; GHD's</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Bars</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Weight, Dumbbells, Kettlebells</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Universal Gyms &amp; Multi Stack</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Lifting Racks &amp; Platforms</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Rigs</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Single Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Selectorized Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Storage Solutions</div>
                                    </div>
                                </a>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="javascript::void(0)">
                                    <img src="/images/mega-menu-banner-1.png" alt="Mega Menu Cardio Banner" width="100%">
                                </a>                            
                            </div>
                        </div>    
                    </div>                    
                </div>
                <div class="tab-pane fade open show" id="v-pills-strength" role="tabpanel" aria-labelledby="v-pills-strength-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Strength Equipment</h5>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="link-underline" href="#">View all Strength Equipment Items</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>                                        
                                        <div class="menu-tile-text">Benches &amp; GHD's</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Bars</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Weight, Dumbbells, Kettlebells</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Universal Gyms &amp; Multi Stack</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Lifting Racks &amp; Platforms</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Rigs</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Single Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Selectorized Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Storage Solutions</div>
                                    </div>
                                </a>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="javascript::void(0)">
                                    <img src="/images/mega-menu-banner-1.png" alt="" width="100%">
                                </a>                            
                            </div>
                        </div>                         
                    </div>
                </div>
                <div class="tab-pane fade open show" id="v-pills-fitness" role="tabpanel" aria-labelledby="v-pills-fitness-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Fitness Accessories</h5>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="link-underline" href="#">View all Fitness Accessories</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Benches &amp; GHD's</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Bars</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Weight, Dumbbells, Kettlebells</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Universal Gyms &amp; Multi Stack</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Lifting Racks &amp; Platforms</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Rigs</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Single Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Selectorized Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Storage Solutions</div>
                                    </div>
                                </a>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="javascript::void(0)">
                                    <img src="/images/mega-menu-banner-1.png" alt="" width="100%">
                                </a>                            
                            </div>
                        </div>                         
                    </div>
                </div>
                <div class="tab-pane fade open show" id="v-pills-conditioning" role="tabpanel" aria-labelledby="v-pills-conditioning-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Strength &amp; Conditioning</h5>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="link-underline" href="#">View all Strength Equipment Items</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Benches &amp; GHD's</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Bars</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Weight, Dumbbells, Kettlebells</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Universal Gyms &amp; Multi Stack</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Lifting Racks &amp; Platforms</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Rigs</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Single Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Selectorized Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Storage Solutions</div>
                                    </div>
                                </a>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="javascript::void(0)">
                                    <img src="/images/mega-menu-banner-1.png" alt="" width="100%">
                                </a>                            
                            </div>
                        </div>                         
                    </div>
                </div>
                <div class="tab-pane fade open show" id="v-pills-martial-arts" role="tabpanel" aria-labelledby="v-pills-martial-arts-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Martial Arts &amp; Boxing</h5>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="link-underline" href="#">View all Martial Arts Equipment</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Benches &amp; GHD's</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Bars</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Weight, Dumbbells, Kettlebells</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Universal Gyms &amp; Multi Stack</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Lifting Racks &amp; Platforms</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Rigs</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Single Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Selectorized Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Storage Solutions</div>
                                    </div>
                                </a>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="javascript::void(0)">
                                    <img src="/images/mega-menu-banner-1.png" alt="" width="100%">
                                </a>                            
                            </div>
                        </div>                         
                    </div>
                </div>
                <div class="tab-pane fade open show" id="v-pills-flooring" role="tabpanel" aria-labelledby="v-pills-flooring-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Flooring</h5>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="link-underline" href="#">View all Flooring Items</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Benches &amp; GHD's</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Bars</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Weight, Dumbbells, Kettlebells</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Universal Gyms &amp; Multi Stack</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');"> 
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Lifting Racks &amp; Platforms</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Rigs</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');"> 
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Single Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Selectorized Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Storage Solutions</div>
                                    </div>
                                </a>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="javascript::void(0)">
                                    <img src="/images/mega-menu-banner-1.png" alt="" width="100%">
                                </a>                            
                            </div>
                        </div>                         
                    </div>
                </div>
                <div class="tab-pane fade open show" id="v-pills-clearance" role="tabpanel" aria-labelledby="v-pills-clearance-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Clearance &amp; Sale</h5>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="link-underline" href="#">View all Clearance &amp; Sale Items</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Benches &amp; GHD's</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Bars</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Weight, Dumbbells, Kettlebells</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Universal Gyms &amp; Multi Stack</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>                                        
                                        <div class="menu-tile-text">Lifting Racks &amp; Platforms</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Rigs</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Single Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Selectorized Stations</div>
                                    </div>
                                </a>                            
                            </div>
                            <div class="col-sm-3">
                                <a href="">
                                    <div class="mega-menu-tile d-flex align-items-center justify-content-center text-center" style="background: url('../../images/mega-menu-item-1.png');">
                                        <div class="overlay"></div>
                                        <div class="menu-tile-text">Storage Solutions</div>
                                    </div>
                                </a>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="javascript::void(0)">
                                    <img src="/images/mega-menu-banner-1.png" alt="" width="100%">
                                </a>                            
                            </div>
                        </div>                         
                    </div>
                </div>
            </div>     
        </div>    
    </nav>
    <div class="nav-bottom-container collapse" id="js-nav-bottom">
        <nav class="navbar navbar-expand-lg bg-light">
            <div class="container">
                <ul class="nav nav-bottom justify-content-between">
                    <li class="nav-item"><a class="nav-link" href="">about us</a></li>
                    <li class="nav-item"><a class="nav-link" href="">blog</a></li>
                    <li class="nav-item"><a class="nav-link" href="">installation services</a></li>
                    <li class="nav-item"><a class="nav-link" href="">shipping</a></li>
                    <li class="nav-item"><a class="nav-link" href="">financing</a></li>
                    <li class="nav-item"><a class="nav-link" href="">careers</a></li>
                    <li class="nav-item"><a class="nav-link" href="">partner program</a></li>                
                </ul>                        
            </div>
        </nav>
    </div>
</header>