// -----------------------------------------------------------------------------------

// Plugins

var gulp       = require('gulp');
var uglify     = require('gulp-uglify');
var sass       = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var minify     = require('gulp-clean-css');
var plumber    = require('gulp-plumber');
var notify     = require('gulp-notify');
var tinypng    = require('gulp-tinypng');
var merge 	   = require('merge-stream');
var concat 	   = require('gulp-concat');


// -----------------------------------------------------------------------------------

// File Destination Variables

// Note: Update js_prod_dest & sass_prod_dest to Laravel instalation when needed

var folder = {
	css_src: 'resources/assets/css/**/*.css',
	css_dest: 'public/css',
	js_src: 'resources/assets/js/**/*.js',
	js_dest: 'public/js',
	js_prod_dest: 'public/js',
	sass_src: 'resources/assets/sass/**/*.scss',
	sass_dest: 'public/css',
	sass_prod_dest: 'public/css',
	image_src: 'public/images/raw/*.png',
	image_dest: 'public/images'
};

// Complie Javascript

gulp.task('scripts', function() {
	var importJsStream = gulp.src('node_modules/bootstrap/dist/js/bootstrap.js')
	        .pipe(concat('imported-js-files.js'));

    var jsStream = gulp.src(folder.js_src)
        .pipe(concat('js-files.js'));

    var mergedJsStream = merge(importJsStream, jsStream)
        .pipe(concat('app.js'))
        // .pipe(minify())
        .pipe(uglify())
        .pipe(gulp.dest(folder.js_dest))
        //.pipe(gulp.dest(folder.js_prod_dest))
        .pipe(notify('JS Compiled!'));

    return mergedJsStream;
});

// -----------------------------------------------------------------------------------

// Compile CSS

gulp.task('styles', function() {

	var cssStream = gulp.src([
			folder.css_src,
			'node_modules/bootstrap/dist/css/bootstrap.css',
		])
        .pipe(concat('css-files.css'));

    var scssStream = gulp.src(folder.sass_src)
    	.pipe(sourcemaps.init())
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(concat('scss-files.css'));

    var mergedStream = merge(cssStream, scssStream)
        .pipe(concat('app.css'))
        .pipe(minify())
        .pipe(gulp.dest(folder.sass_dest))
        //.pipe(gulp.dest(folder.sass_prod_dest))
        .pipe(notify('Sass compiled!'));

    return mergedStream;
});


// -----------------------------------------------------------------------------------

// Minify CSS Only 

gulp.task('minify', function() {
	
	gulp.src('css/*.css')
	.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
	.pipe(minify({
		advanced: false
	}))
	.pipe(gulp.dest('css'))
	.pipe(notify('CSS Minified!'));

});

// -----------------------------------------------------------------------------------

// Compress PNGs

gulp.task('png', function(){

	gulp.src(folder.image_src)
	.pipe(tinypng('lllWdDv1wRmdGpsF1yazpPYF6vZbgMqJ'))
	.pipe(gulp.dest(folder.image_dest))
	.pipe(notify('PNG Images compressed'));

});

// -----------------------------------------------------------------------------------

// Watch Task

gulp.task('watch', function() {
	gulp.watch(folder.js_src, ['scripts']);
	gulp.watch(folder.sass_src, ['styles']);
});

// -----------------------------------------------------------------------------------

// Default Task

gulp.task('default', ['scripts', 'styles', 'watch']);

// -----------------------------------------------------------------------------------

// Build Task

gulp.task('build', ['png', 'minify']);