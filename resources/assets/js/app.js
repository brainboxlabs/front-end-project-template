"use strict";
$(function() {

    // Search Box
    var search_value = $('.js-search').val();
    $('.js-search').click(function(){
        $(this).addClass('js-search-expand');
        $('.navbar .input-group-text').addClass('input-group-text-expanded');
        $('#search-icon').css('stroke', '#565656');
    });
    
    $('.js-close-cta').click(function(){
        $('.js-search').val(search_value);        
        $('.nav-cta').css('display', 'none');        
    });

    window.onclick = function(e) {
        if (!e.target.matches('.js-search-expand')){            
            $('.js-search').removeClass('js-search-expand');            
            $('.navbar .input-group-text').removeClass('input-group-text-expanded');
            $('.js-search').val("");
            $('#search-icon').css('stroke', '#ffffff');
        }
    };       

    // Hamburger
    $('.hamburger').click(function() {        
        $(this).toggleClass('is-active');
    });


    // // Mega Menu
    // $('#js-mega-menu-trig').click(function(e) {        
    //     e.preventDefault();
    //     $('#v-pills-cardio-tab').tab('show');
    //     $('#js-mega-menu-dropdown').dropdown('toggle');	
    // });


    // Mega Menu Steps

    // 1. When the mega menu opens both the dropdown the first panel ('cardio) on click of the dropdown
    $('#js-mega-menu-trig').on('click', function(event) {        
        if($('.js-mega-menu-class').hasClass("show")){                        
        } else {
            if($('.tab-pane').hasClass('active')){                
                $('.tab-pane').removeClass('active');
                $('.mega-dropdown').removeClass('show');    
            } else {
                $('.mega-dropdown').addClass('show');
            }            
        }        
    });
    // 2. When you click on another tab - the mega menu opens the next tab. However, the dropdown does not close
    // 3. If the mega menu is open and the user clicks away from it -  close everything



    // (function(){
    //     var a = b = 3;
    // })();

    // console.log(a);
    // console.log(b);
});